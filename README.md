## Why Utilities?

These utilities class is write for **JAVA** application which need a useful utilities for execute query and date
converter for any timezone:

- [**Sql utilities**](#sql-utilities) : provide a useful class for execute raw query from resource clean, readable
- [**Date utilities**](#date-utilities) : provide a useful converter class for convert date for any timezone

## Installation

In your project add the dependency of the library:

### Apache Maven

```xml
<dependency>
    <groupId>io.gitlab.khoem.sombath</groupId>
    <artifactId>utils</artifactId>
    <version>1.0.3</version>
</dependency>
```

### Gradle Groovy DSL

```groovy
implementation 'io.gitlab.khoem.sombath:utils:1.0.3'
```

### Gradle Kotlin DSL

```kotlin
implementation("io.gitlab.khoem.sombath:utils:1.0.3")
```

---

## Usages

after adding dependency you need to provide bean configuration for **SqlUtilities** as following

```java
@Configuration
@RequiredArgsConstructor
public class GlobalConfig {

    private final DataSource dataSource;

    @Bean
    public SqlUtility provideSqlUtils() {
        return new SqlUtility(dataSource);
    }
}
```

### Sql utilities

after adding **Bean** for sql utils you can use it as following example

```java
@Service
@RequiredArgsConstructor
public class ReportService {

    private final SqlUtility sqlUtility;

    @Value("classpath:query/report.sql")
    Resource reportResource;

    // execute update
    public void updateReport() {
        sqlUtility.executeQuery(reportResource);
    }

    // execute update with param
    public void updateReport() {
        Map<String, Object> params = Map.of("userCode", userCode);
        sqlUtility.executeQuery(reportResource, params);
    }

    // list
    public List<ReportTO> getReport() {
        return sqlUtility.executeQueryForList(reportResource, ReportTO.clas);
    }

    // list with param
    public List<ReportTO> getReport(String userCode) {
        Map<String, Object> params = Map.of("userCode", userCode);
        return sqlUtility.executeQueryForList(reportResource, params, ReportTO.clas);
    }

    // Page
    public Page<ReportTO> getReport(Integer page, Integer size, String userCode) {
        PageRequest pageRequest = PageRequest.of(page, size);
        Map<String, Object> params = Map.of("userCode", userCode);
        return sqlUtility.executeQueryForPage(reportResource, pageRequest, params, ReportTO.clas);
    }
}
```

### Date utilities

DateUtils class provide some constant variable:

- `DATE_TIME_FORMAT: yyyy-MM-dd HH:mm:ss`
- `DATE_FORMAT: yyyy-MM-dd`
- `TIME_FORMAT: HH:mm:ss`

ZoneId:

- `ZoneID.UTC.getName(): UTC`
- `ZoneID.KH.getName(): Asia/Phnom_Penh`
- `ZoneID.TH.getName(): Asia/Bangkok`
- `ZoneID.VN.getName(): Asia/Vientiane`

for **Date** utils you can use it as following example

```java
@Service
@RequiredArgsConstructor
public class ReportService {

    String date;

    public void displayDate() {

        date = DateUtils.getDate("UTC", "yyyy-MM-dd hh:mm:ss a");
        System.out.println(date); // = "2022-08-31 07:13:19 AM"

        date = DateUtils.getDate("Asia/Phnom_Penh", "yyyy-MM-dd hh:mm:ss a");
        System.out.println(date); // = "2022-08-31 02:13:19 PM"

        // convert instant to specific format with specific timezone
        date = DateUtils.getDate(Instant.now(), ZoneID.UTC.getName(), DateUtils.TIME_FORMAT);
        System.out.println(date); // = "14:13:19"

        // convert string date from a timezone to another String date timezone
        date = DateUtils.getDate("2022-08-31 02:13:19 PM", "yyyy-MM-dd hh:mm:ss a", "Asia/Phnom_Penh", "UTC", "yyyy-MM-dd hh:mm:ss a");
        System.out.println(date); // = "2022-08-31 07:13:19 AM"

    }
}
```

### Additional resource

- [Spring JPA](https://spring.io/guides/gs/accessing-data-jpa/)
- [Spring JDBC](https://www.baeldung.com/spring-jdbc-jdbctemplate)
- [Available Zone ID](https://docs.oracle.com/cd/E84527_01/wcs/tag-ref/MISC/TimeZones.html) 