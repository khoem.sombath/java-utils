package com.sombath.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Sombath
 * create at 24/8/22 3:44 PM
 */

public class DateUtils {

    /**
     * The standard format date and time.
     */
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";

    /**
     * The standard start of date and end of day in format HH:mm:ss.
     */
    public static final String START_OF_DAY = " 00:00:00";
    public static final String END_OF_DAY = " 23:59:59";

    /**
     * <pre>
     * DateUtils.getDate("UTC", "yyyy-MM-dd") = "2022-08-31"
     * DateUtils.getDate("UTC", "hh:mm:ss a") = "02:02:40 PM"
     * </pre>
     *
     * @param zoneId String zone or localization
     * @param format String the format of date sth like "yyyy-mm-dd" ...
     * @return String
     */
    public static String getDate(String zoneId, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(zoneId));
        return dateTimeFormatter.format(Instant.now());
    }

    /**
     * @param instant Instant
     * @param zoneId  String
     * @param format  String
     * @return String
     */
    public static String getDate(Instant instant, String zoneId, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(zoneId));
        return dateTimeFormatter.format(instant);
    }

    /**
     * @param instant Instant
     * @param format  String
     * @return String
     */
    public static String getDate(Instant instant, String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.systemDefault());
        return dateTimeFormatter.format(instant);
    }

    /**
     * @param dateTimeString String
     * @param format         Sting
     * @param inputZoneId    String
     * @param outputZoneId   String
     * @return Instant
     */
    public static Instant getDate(String dateTimeString, String format, String inputZoneId, String outputZoneId) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(inputZoneId));
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTimeString, dateTimeFormatter);
        Instant instant = zonedDateTime.toInstant();
        return instant.atZone(ZoneId.of(outputZoneId)).toInstant();
    }

    /**
     * <pre>
     * DateUtils.getDate("2022-08-31 02:13:19 PM", "yyyy-MM-dd hh:mm:ss a", "Asia/Phnom_Penh", "UTC", "yyyy-MM-dd hh:mm:ss a") = "2022-08-31 07:13:19 AM"
     * </pre>
     *
     * @param dateTimeString String date time in string format
     * @param format         Sting date time format
     * @param inputZoneId    String input date time string zoned id
     * @param outputZoneId   String output that you want in zoned id
     * @param outPutFormat   String output date format
     * @return String
     */
    public static String getDate(String dateTimeString, String format, String inputZoneId, String outputZoneId, String outPutFormat) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(inputZoneId));
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTimeString, dateTimeFormatter);
        Instant instant = zonedDateTime.toInstant();
        dateTimeFormatter = DateTimeFormatter.ofPattern(outPutFormat).withZone(ZoneId.of(outputZoneId));
        return dateTimeFormatter.format(instant);
    }

    /**
     * @param dateString String
     * @param format     String
     * @param zoneId     String
     * @return Instant
     */
    public static Instant getDate(String dateString, String format, String zoneId) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of(zoneId));
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateString, dateTimeFormatter);
        return zonedDateTime.toInstant();
    }
}
