package com.sombath.utils;

/**
 * @author Sombath
 * create at 30/8/22 3:31 PM
 */
public enum ZoneID {

    UTC("UTC"),
    KH("Asia/Phnom_Penh"),
    TH("Asia/Bangkok"),
    VN("Asia/Vientiane");

    private final String name;

    ZoneID(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
